#! /usr/bin/env python

import sys
import copy
import rospy
import numpy as np
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from tf import transformations
from math import cos, sin, pi


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_phython_tut', anonymous=True)

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group = moveit_commander.MoveGroupCommander("manipulator")
display_trajectory_publisher = rospy.Publisher('move_group/display_planned_path',moveit_msgs.msg.DisplayTrajectory)

pose_target = geometry_msgs.msg.Pose()

q = transformations.quaternion_from_euler(0, 0, 0, "sxyz") # r p y

print "ref frame: %s" % group.get_planning_frame()

print "joint values: %s" % group.get_current_joint_values()

print "end effector pose: %s" % group.get_current_pose()

print "Robot current state: %s" % robot.get_current_state()

pose_target.orientation.x = q[0]
pose_target.orientation.y = q[1]
pose_target.orientation.z = q[2]
pose_target.orientation.w = q[3]

pose_target.position.x=0.5
pose_target.position.y=0.5
pose_target.position.z=0.5

###########################
#calculation of base frame location from end effector

####end effector location
e_x =group.get_current_pose().pose.position.x
e_y =group.get_current_pose().pose.position.y
e_z =group.get_current_pose().pose.position.z

end_effector_location = np.array([[e_x],[e_y],[e_z],[1.00]])

##dh parameters
d1 =  0.089159
a2 = -0.42500
a3 = -0.39225
d4 =  0.10915
d5 =  0.09465
d6 =  0.0823
a = np.array([0,0,a2,a3,0,0])
d = np.array([d1,0,0,d4,d5,d6])
alpha = np.array([1.57,0,0,1.57,-1.57,0])

#joint angles from theta1-6
theta = group.get_current_joint_values()
#default joint values
#theta = np.array([0.7065433553865974, -0.9388140232246958, 1.007216068133884, 3.0723168516139676, -2.2771523891760097, -0.0009641151009898152])
###kinematics
s1 = sin(theta[0])
c1 = cos(theta[0])
q23 = theta[1]
q234 = theta[1]
s2 = sin(theta[1])
c2 = cos(theta[1])
s3 = sin(theta[2])
c3 = cos(theta[2])
q23 += theta[2]
q234 += theta[2]
s4 = sin(theta[3])
c4 = cos(theta[3])
q234 += theta[3]
s5 = sin(theta[4])
c5 = cos(theta[4])
s6 = sin(theta[5])
c6 = cos(theta[5])
s23 = sin(q23)
c23 = cos(q23)
s234 = sin(q234)
c234 = cos(q234)
###transformation matrix
T = np.array([[c234*c1*s5 - c5*s1,c6*(s1*s5 + c234*c1*c5) - s234*c1*s6,-s6*(s1*s5 + c234*c1*c5) - s234*c1*c6,d6*c234*c1*s5 - a3*c23*c1 - a2*c1*c2 - d6*c5*s1 - d5*s234*c1 - d4*s1], \
			[c1*c5 + c234*s1*s5,-c6*(c1*s5 - c234*c5*s1) - s234*s1*s6,s6*(c1*s5 - c234*c5*s1) - s234*c6*s1,d6*(c1*c5 + c234*s1*s5) + d4*c1 - a3*c23*s1 - a2*c2*s1 - d5*s234*s1],\
			[-s234*s5,-c234*s6 - s234*c5*c6,s234*c5*s6 - c234*c6,d1 + a3*s23 + a2*s2 - d5*(c23*c4 - s23*s4) - d6*s5*(c23*s4 + s23*c4)],\
			[0.0,0.0,0.0,1.0]])
#########to get base location
T_inv = np.linalg.inv(T) 

Base = np.dot(T_inv,end_effector_location)

print "Robot base location: %s" % Base

# group_variable_values = group.get_current_joint_values()

# group_variable_values[0] = 1
# group_variable_values[1] = 0
# group_variable_values[3] = -1.5
# group_variable_values[5] = 1.5

# group.set_joint_value_target(group_variable_values)
group.set_pose_target(pose_target)

plan1 = group.plan()

group.go(wait=True)
# rospy.sleep(5)

moveit_commander.roscpp_shutdown()
